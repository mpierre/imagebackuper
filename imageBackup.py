#! /usr/bin/python

import re
from subprocess import Popen, PIPE
import os
import sys
from pprint import pprint
import codecs
import json


def getFilesFromMdFind(path=None):
    onlyin = ("-onlyin '%s'" % path) if path else ""
    for line in Popen("mdfind %s 'kMDItemPixelWidth >= 600 && kMDItemPixelHeight >= 600'" % onlyin, shell=True, stdout=PIPE).stdout:
        yield line.strip()


def getFoldersByDate(imageFiles):
    dateOptions = ["DateTimeOriginal", "FileModifyDate", "ProfileDateTime"]
    cmd = "exiftool -j -S -d %%Y/%%m/%%d -SourceFile -%s \"%s\"" % (" -".join(dateOptions), '" "'.join(imageFiles))
    rawData = Popen(cmd, shell=True, stdout=PIPE).stdout.read()
    #print(rawData)
    jsonData = json.loads(rawData)
    #print(jsonData)
    data = []

    for de in jsonData:
        for dateOption in dateOptions:
            if dateOption in de and not de[dateOption].startswith("0000"):
                date = de[dateOption]
                break
        if 'date' not in locals():
            continue
        data.append((de['SourceFile'], date))
    return data


def createSymlink(imageFile, folder):
    dest = os.path.join(folder, os.path.basename(imageFile))
    #print "Symlinkning %s to %s" % (imageFile, dest)
    if not os.path.exists(folder):
        os.makedirs(folder)
    if not os.path.lexists(dest):
        os.symlink(imageFile, dest)
    elif not os.path.samefile(dest, imageFile):
        print "Duplicate! Need to do some de-duping here!"
        deDupeAndLink(imageFile, dest)


def deDupeAndLink(imageFile, dest):
    if 'thumb' in imageFile.lower():
        print "Using old file, new is thumbnail"
    elif 'thumb' in os.path.realpath(dest):
        print "Using new file, old is thumbnail"
        os.symlink(imageFile, dest)
    elif md5sum(imageFile) == md5sum(dest):
        print "Same file, not updating"
    else:
        print "Not sure what to do about these:"
        print imageFile
        print os.path.realpath(dest)


def md5sum(imageFile):
    Popen("md5 -q '%s'" % os.path.realpath(imageFile), shell=True, stdout=PIPE).stdout.read()


def processBatch(batch, completedFiles, prog):
    if len(batch) == 0:
        return
    foldersByImage = getFoldersByDate(batch)
    for imageFile, folder in foldersByImage:
        folder = folder.strip()
        if prog.match(folder):
            createSymlink(imageFile, folder)
            completedFiles.write(imageFile)
            completedFiles.write("\n")
        else:
            print "Skipping dir %s - not matching format for image: %s" % (folder, imageFile)
    print "Processed %d images" % len(batch)


def main():
    batch = []
    batchSize = 200
    prog = re.compile("\d\d\d\d/\d\d/\d\d")
    if os.path.exists('complete.txt'):
        doneFiles = set(codecs.open('complete.txt', 'r').read().splitlines())
    else:
        doneFiles = []
    completedFiles = codecs.open('complete.txt', 'a', 'utf-8')
    path = sys.argv[1] if len(sys.argv) > 1 else None
    for image in getFilesFromMdFind(path):
        if image not in doneFiles:
            batch.append(image)
        if len(batch) >= batchSize:
            processBatch(batch, completedFiles, prog)
            batch = []
            completedFiles.flush()
    processBatch(batch, completedFiles, prog)
    completedFiles.close()


if __name__ == "__main__":
    main()
